// A proxy notification service
var Notify = angular.module('Notify', []);

Notify.service('notify', [function(){
    var that = this;
    this.alert = function(msg, callback, title, buttons) {
        if (navigator.notification !== undefined) {
            navigator.notification.alert(msg, callback, title, buttons);
        }
        else {
            alert(msg);
        }
    };

    this.confirm = function(msg, callback, title, buttons) {
        if (navigator.notification !== undefined) {
            navigator.notification.confirm(msg, callback, title, buttons);
        }
        else {
            confirm(msg);
        }
    };

    this.exit = function() {
        msg = "آیا می‌خواهید از برنامه خارج شوید؟";
        callback = function(btn){
            if (btn == 1)
             navigator.app.exitApp();
        };
        buttons = "بله,خیر";
        title = "";
        this.confirm(msg, callback, title, buttons);

    };
}]);
