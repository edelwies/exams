// A fake ORM module that provides the orm service
var ORM = angular.module('ORM', ["Notify"]);

ORM.service('orm', ["$http", "notify", function($http, notify){
    var that = this;

    this.init = function(){
        that._data = that._data || [];

        //that._answers = that._answers || [];
        that._answers = [];
        $.ajax({
            url: 'db/data',
            dataType: "json",
            async: false
        }).success(function(data){
            if (this._data != []) {
                that._data = data;
                console.log('Data loaded');
            }
            else { console.log('Data already loaded');}
        }).error(function(){
            notify.alert('Loading failed.', function(){}, 'Faild', 'OK');
        });
    };

    this.correct_answers = function(){
        console.log(that._data.pluckMany( "id", "correct_answer").value());
        console.log("-__-_--");
        return that._data.pluckMany( "id", "correct_answer").value();
    };
    this.find = function(id) {
        return _.find(that._data, function(x){
            return x.id == id;
        });
    };

    this.set_answer  = function(id, answer, answer_index) {
        that._answers.push([id, answer, answer_index]);
    };

    this.answers = function(){
        return that._answers;
    };
    return this;
}]);
