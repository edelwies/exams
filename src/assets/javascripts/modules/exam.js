// Exam Module
// -----------------------
// This module is responsible for application settings
var Exam = angular.module("Exam", ["gettext", "ORM", "ionic", "Notify"]);

// configuration section ---------------------------
Exam.config(["$stateProvider", function($stateProvider){

    // Configuring application index route.
    // Add any route you need here.
    $stateProvider.
        state("exams", {
            url: "/start/:id",
            templateUrl: template_url("exam/index"),
            controller: "ExamsController"
        }).
        state("result", {
            url: "/result/",
            templateUrl: template_url("exam/result"),
            controller: "ResultExamsController"
        }).
        state("description", {
            url: "/description",
            templateUrl: template_url("exam/description"),
            controller: "DescController"
        });


}]);

Exam.controller("ExamsController", ["$scope", "gettext", "orm", "notify", "$location", "$stateParams", function($scope, gettext, orm, $notify, $location, $stateParams){
    // Exam controller of application. This controller is responsible for `/game` url


    // -----------------------------------

    $scope.get_question = function(id) {
        $scope.question = orm.find(id);

        if($scope.question === undefined ) {
            $scope.process_answers();
        }
    };


    $scope.data = {
        clientSide: 'ng'
    };

    $scope.set_user_answer = function(answer){
        $scope.user_answer = answer;
        var next_id = parseInt(id) + 1;

    };

    $scope.correct = function(id) {
        orm.set_answer(id,
                       "درست",
                       0);
        var next_id = parseInt(id) + 1;
        $location.path("/start/" + next_id);

    };

    $scope.not_correct = function(id) {
        orm.set_answer(id,
                       "غلط",
                       1);
        var next_id = parseInt(id) + 1;
        $location.path("/start/" + next_id);

    };

    $scope.next = function(id) {
        if ( $scope.user_answer === undefined ){
            $notify.alert("یکی از گزینه‌ها را انتخاب کنید", undefined ,"خطا","تایید");
        }else{
            orm.set_answer(id, $scope.user_answer, _.indexOf($scope.question.answers, $scope.user_answer));
            var next_id = parseInt(id) + 1;
            $location.path("/start/" + next_id);
        }
    };

    $scope.process_answers = function(){
        var answers = orm.answers();
        $location.path("/result/");
    };

    $scope.get_question($stateParams.id);

}]);


Exam.controller("ResultExamsController", ["$scope", "gettext", "orm", "notify", "$location", "$stateParams", function($scope, gettext, orm, $notify, $location, $stateParams){
    // Exam controller of application. This controller is responsible for `/game` url

    var count = 0;
    _.each(orm.answers(), function(x) {
        var question = orm.find(x[0]);
        if (question.correct_answers == x[2]){
            count = count + 1;
        }

    });
    count = count * 10;
    $scope.results = count + "%";
    $(".fcontainer").height(window.screen.height / 2.5);
}]);

Exam.controller("DescController", ["$scope", "gettext", "orm", "notify", "$location", "$stateParams", "$ionicScrollDelegate", function($scope, gettext, orm, $notify, $location, $stateParams,  $ionicScrollDelegate){

}]);
