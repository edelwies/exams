require 'yaml'
require 'json'
data = YAML.load_file ARGV[0]

data.each do |game|
  game[:first_row], *rows = game["data"].split(";")
  game[:first_row] = game[:first_row].split(',')
  game[:rest_of_rows] = rows.map do |row|
    row = row.split(',')
    row << ''
    row
  end

end

puts JSON.pretty_generate(data)
